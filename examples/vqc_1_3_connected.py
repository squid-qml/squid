from squid.models import FeedForward, MainModel, VQC

import torch
from torch.optim import SGD
from torch.nn import CrossEntropyLoss


# Hyperparameters
num_examples = 1000
num_features = 10
batch_size = 32
epochs = 20

# Make data
x = torch.randn((num_examples, num_features))
y = (1.0 * (torch.sum(x, dim=1) > 0.0)).long()

# Define model, loss and optimizer
model = MainModel(
    FeedForward([10, "relu", 9, "relu", 6], in_features=num_features),
    VQC(6, 2),
    FeedForward([], in_features=2**2, num_classes=2),
)
opt = SGD(model.parameters(), lr=0.01, momentum=0.0)
criterion = CrossEntropyLoss()

for epoch in range(epochs):
    running_loss = 0.0
    idxs = torch.randperm(num_examples)
    num_batches = num_examples // batch_size + ((num_examples % batch_size) > 0)
    for i in range(num_batches):
        batch_idxs = idxs[i * batch_size:(i + 1) * batch_size]
        batch_x = x[batch_idxs]
        batch_y = y[batch_idxs]

        opt.zero_grad()

        y_pred = model(batch_x)
        loss = criterion(y_pred, batch_y)

        loss.backward()
        model.backward()
        opt.step()

        running_loss += len(batch_idxs) * loss.item()

    print(f"Epoch: {epoch}\tLoss: {running_loss / num_examples:.4g}")
