from squid.models import ConvFeedForward, FeedForward, MainModel, VQC

import torch
from torch.optim import SGD
from torch.nn import CrossEntropyLoss


# Hyperparameters
num_examples = 1000
batch_size = 32
epochs = 20

# Make data
x = torch.randn((num_examples, 3, 24, 24))
y = (1.0 * (torch.sum(x, dim=(1, 2, 3)) > 0.0)).long()

# Define model, loss and optimizer
model = MainModel(
    ConvFeedForward(
        in_shape=[3, 24, 24],
        conv_layers=[("C2", 5, 3), ("M2", 2), ("C2", 5, 5), ("M2", 2), ("C2", 15, 3)],
        layers=[],
    ),
    VQC(15, 2),
    FeedForward([], in_features=2**2, num_classes=2),
)
opt = SGD(model.parameters(), lr=0.001, momentum=0.0)
criterion = CrossEntropyLoss()

for epoch in range(epochs):
    running_loss = 0.0
    idxs = torch.randperm(num_examples)
    num_batches = num_examples // batch_size + ((num_examples % batch_size) > 0)
    for i in range(num_batches):
        batch_idxs = idxs[i * batch_size:(i + 1) * batch_size]
        batch_x = x[batch_idxs]
        batch_y = y[batch_idxs]

        opt.zero_grad()

        y_pred = model(batch_x)
        loss = criterion(y_pred, batch_y)

        loss.backward()
        model.backward()
        opt.step()

        running_loss += len(batch_idxs) * loss.item()

    print(f"Epoch: {epoch}\tLoss: {running_loss / num_examples:.4g}")
