import invoke
import shlex


TEST_COVERAGE_FAIL = 85


def __read_meta_yaml(ctx):
    try:
        import yaml
    except:
        ctx.run("pip install pyyaml")

    import yaml

    with open("meta.yaml", mode="r") as f:
        packages = yaml.safe_load(f)
    return packages


@invoke.task
def install(ctx):
    """Installs squid"""
    ctx.run("python -m pip install .")


@invoke.task
def develop(ctx, pip=False):
    """Install squid in development mode."""
    packages = __read_meta_yaml(ctx)["requirements"]

    secure_packages = [shlex.quote(x) for x in packages["develop"]]
    if pip:
        secure_packages = [x.replace(" ", "==") for x in secure_packages]
        ctx.run("pip install " + " ".join(secure_packages), echo=True)
    else:
        ctx.run("conda install --yes " + " ".join(secure_packages), echo=True)

    secure_packages = [shlex.quote(x) for x in packages["develop-pip"]]
    secure_packages = [x.replace(" ", ">=") for x in secure_packages]
    ctx.run("pip install " + " ".join(secure_packages), echo=True)

    ctx.run("python -m pip install -e .")  # Might need to add --no-use-pep517, if something breaks


@invoke.task
def install_required_packages(ctx, pip=False):
    ctx.run("pip install -r requirements.txt")


@invoke.task
def doc(ctx):
    ctx.run("cd docs && make clean && make html")


@invoke.task
def publish(ctx):
    ctx.run("rm -rf dist/")
    ctx.run("python setup.py sdist bdist_wheel")
    ctx.run("twine check dist/*")
    ctx.run("twine upload dist/*")


@invoke.task(optional=["apply"])
def lint(ctx, apply=False):
    ctx.run("flake8 squid tests", echo=True)

    isort_flags = ["--diff", "--check-only"]
    black_flags = ["--diff"]

    if apply:
        isort_flags = black_flags = []

    isort_flags = " ".join(isort_flags)
    black_flags = " ".join(black_flags)

    ctx.run(
        f"isort {isort_flags} squid tests", echo=True
    )
    result = ctx.run(
        f"black {black_flags} squid tests", echo=True
    )
    if "reformatted" not in result.stderr:
        ctx.run("mypy --no-incremental --cache-dir /dev/null squid", echo=True)


@invoke.task(
    help={
        "integration": "Whether to run integration tests or not. (Default: False)",
        "junitxml": "Create junit-xml style report file at given path. (Default: None)",
    },
)
def test(ctx, integration=False, junitxml=None):
    if integration:
        flags = "-m integration"
    else:
        flags = f"--cov=squid --cov-fail-under={TEST_COVERAGE_FAIL}"

    if junitxml is not None:
        flags += f" --junitxml={junitxml}"

    ctx.run("pytest " + flags, echo=True)
