from torch import nn

from .to_range import ToRange


def str_to_activation(activation: str) -> nn.Module:
    """Converts string to PyTorch's activation.
    Currently:

    .. list-table:: Output classes corresponding to input strings
        :widths: 15 15
        :header-rows: 1

        * - activation
          - output
        * - :code:`relu`
          - :code:`nn.ReLU()`
        * - :code:`sigmoid`
          - :code:`nn.Sigmoid()`
        * - :code:`tanh`
          - :code:`nn.Tanh()`
        * - :code:`leaky relu`, :code:`leaky-relu`, :code:`leaky_relu`
          - :code:`nn.LeakyReLU()`
        * - :code:`angle`, :code:`to-angle`, :code:`range`, :code:`to-range`
          - :py:class:`ToRange`

    :param activation: String representation of activation
    :type activation: str
    :raises ValueError: Error is thrown if activation string is not recognized.
    :return: PyTorch module for corresponding activation function with default values.
    :rtype: nn.Module
    """
    if activation.lower() in {"relu"}:
        return nn.ReLU()
    elif activation.lower() in {"sigmoid"}:
        return nn.Sigmoid()
    elif activation.lower() in {"tanh"}:
        return nn.Tanh()
    elif activation.lower() in {"leaky relu", "leaky-relu", "leaky_relu"}:
        return nn.LeakyReLU()
    elif activation.lower() in {
        "angle",
        "to angle",
        "to_angle",
        "to-angle",
        "range",
        "to range",
        "to_range",
        "to-range",
    }:
        return ToRange()
    else:
        raise ValueError("Unrecognized activation argument.")
