import numpy as np

# 1 qubit Pauli
p0 = np.identity(2)
p1 = np.array([[0.0, 1.0], [1.0, 0.0]])
p2 = np.array([[0.0, -1j], [1j, 0.0]])
p3 = np.array([[1.0, 0.0], [0.0, -1.0]])
# 2 qubit Pauli needed for U4
p11 = np.kron(p1, p1)
p22 = np.kron(p2, p2)
p33 = np.kron(p3, p3)

# magic basis
# mb = (1./np.sqrt(2.))*np.array([[1.,1j,0.,0.], [0.,0.,1j,1.], [0.,0.,1j,-1.], [1.,-1j,0.,0.]])
# mbH = (1./np.sqrt(2.))*np.array([[1.,0,0.,1.], [-1j,0.,0.,1j], [0.,-1j,-1j,0], [0.,1.,-1.,0.]])


def get_basic_u4(params):
    # TODO: add gradients in here like for u2

    # using a rotation to the magic basis to simplify this
    diagvals = [
        np.exp(1j * (params[0] - params[1] + params[2])),
        np.exp(1j * (-params[0] + params[1] + params[2])),
        np.exp(1j * (params[0] + params[1] - params[2])),
        np.exp(-1j * (params[0] + params[1] + params[2])),
    ]

    p01 = 0.5 * (diagvals[0] + diagvals[1])
    m01 = 0.5 * (diagvals[0] - diagvals[1])
    p23 = 0.5 * (diagvals[2] + diagvals[3])
    m23 = 0.5 * (diagvals[2] - diagvals[3])

    dmat = np.array(
        [
            [p01, 0.0, 0.0, m01],
            [0.0, p23, m23, 0.0],
            [0.0, m23, p23, 0.0],
            [m01, 0.0, 0.0, p01],
        ]
    )

    return dmat


def get_u2_mat_and_grad(params):
    """

    :param params: 2 3-dimensional lists:
        - the first one is a list of the 3 sines
        - the second is a list of the 3 cosines
    :return: the bare unitary plus a list containing the 3 unitaries associated with the gradients:
        - U_0(x,y,z) = U_0(x + \pi / 2, y, z)
        - U_1(x,y,z) = U_0(x, y + \pi / 2, z)
        - U_2(x,y,z) = U_0(x, y, z + \pi / 2)
    """
    # IN: params contains 2 3-dimensional lists:
    #        - the first one is a list of the 3 sines
    #        - the second is a list of the 3 cosines
    # OUT: the routine should return the bare unitary
    #             plus a list containing the 3 unitaries associated
    #             with the gradients:
    #
    #             U_0(x,y,z) = U_0(x+\pi/2,y,z)
    #             U_1(x,y,z) = U_0(x,y+\pi/2,z)
    #             U_2(x,y,z) = U_0(x,y,z+\pi/2)
    #

    # build bare unitary first
    az = params[1][1] + 1j * params[0][1]
    azc = params[1][1] - 1j * params[0][1]
    # compute matrix elements
    u00 = az * params[1][0] * params[1][2] - azc * params[0][0] * params[0][2]
    u01 = az * params[1][0] * params[0][2] + azc * params[0][0] * params[1][2]
    u10 = -u01.conjugate()
    u11 = u00.conjugate()

    # build final unitary
    u2_mat = np.array([[u00, u01], [u10, u11]])

    u2_glist = []
    dmat = np.array([[u10, u11], [-u00, -u01]])
    u2_glist.append(dmat)

    az = -params[0][1] + 1j * params[1][1]
    azc = -params[0][1] - 1j * params[1][1]
    # compute matrix elements
    d00 = az * params[1][0] * params[1][2] - azc * params[0][0] * params[0][2]
    d01 = az * params[1][0] * params[0][2] + azc * params[0][0] * params[1][2]
    d10 = -d01.conjugate()
    d11 = d00.conjugate()
    # build final unitary
    dd_mat = np.array([[d00, d01], [d10, d11]])
    u2_glist.append(dd_mat)

    dmat2 = np.array([[-u01, u00], [-u11, u10]])
    u2_glist.append(dmat2)

    return u2_mat, u2_glist
