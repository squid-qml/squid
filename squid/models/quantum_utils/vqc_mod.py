from typing import Tuple

import numpy as np

from .vqc_unitaries import get_basic_u4, get_u2_mat_and_grad, p0

# | number of qubits | number of angles | u2 layers | u4 layers |
# -------------------|------------------|-----------|-----------|
# | 2                | 6                | 1         | 0         |
# | 2                | 9                | 1         | 1         |
# | 4                | 12               | 1         | 0         |
# | 2                | 15               | 2         | 1         |
# | 6                | 18               | 1         | 0         |
# | 4                | 30               | 2         | 1         |
# | 4                | 39               | 3         | 2         |
# | 6                | 45               | 2         | 1         |
# | 4                | 57               | 4         | 3         |
# -------------------|------------------|-----------|-----------|


def validate_model(num_angles: int, num_qubits: int) -> Tuple[int, int]:
    """Validates whether a provided number of angels is currently supported by VQC.
        Sometimes, number of qubits is required to get the specific number of layers.

    :param num_angles: Number of angles provided to the VQC model.
    :type num_angles: int
    :param num_qubits: Number of qubits to be used in the VQC model.
    :type num_qubits: int
    :raises ValueError: If the num_angles is not recognized,
        or if num_angles and num_qubits doesn't match.
    :return: Number of u2 and u4 layers, respectively.
    :rtype: Tuple[int, int]
    """
    if num_angles == 15:
        rec_qubits = 2
        u2layers = 2
        u4layers = 1
    elif num_angles == 6:
        rec_qubits = 2
        u2layers = 1
        u4layers = 0
    elif num_angles == 3:
        rec_qubits = 1
        u2layers = 1
        u4layers = 0
    elif num_angles == 9:  # NOTE: probably useless
        rec_qubits = 2
        u2layers = 1
        u4layers = 1
    elif num_angles == 12:
        rec_qubits = 4
        u2layers = 1
        u4layers = 0
    elif num_angles == 18:
        rec_qubits = 6
        u2layers = 1
        u4layers = 0
    elif num_angles == 30:
        rec_qubits = 4
        u2layers = 2
        u4layers = 1
    elif num_angles == 39:
        rec_qubits = 4
        u2layers = 3
        u4layers = 2
    elif num_angles == 45:
        rec_qubits = 6
        u2layers = 2
        u4layers = 1
    elif num_angles == 57:
        rec_qubits = 4
        u2layers = 4
        u4layers = 3
    else:
        raise ValueError("Don't know this parametrization")
    if num_qubits is not None and rec_qubits != num_qubits:
        raise ValueError("Parametrization not compatible with input num_qubits")

    return u2layers, u4layers


def measure_vqc_output(angles: np.ndarray, vqc_params, do_grad=True):

    num_quant_params, num_qubits, u2layers, u4layers = vqc_params
    num_angles = len(angles)
    if num_angles != num_quant_params:
        raise ValueError("Invalid number of angles in the input of vqc")

    # precompute trig functions
    vec_sin = np.sin(angles)
    vec_cos = np.cos(angles)

    rsize = 1 << num_qubits
    in_vec = np.zeros(rsize)
    in_vec[0] = 1.0

    tot_layers = u2layers + u4layers

    if u2layers > 2:
        u2skip = True
    else:
        u2skip = False

    u4_idx = 0
    u2_idx = 0
    jnow = 0
    vlist_now = [in_vec]
    for layer_idx in range(tot_layers):
        if layer_idx % 2 == 1:  # u4 layer
            if u4_idx % 2:  # odd layer
                num_unitaries = num_qubits // 2 - 1
                do_first = False
            else:  # even layer
                num_unitaries = num_qubits // 2
                do_first = True

            u4_idx += 1

            # get parameters
            layer_parameters = [
                angles[3 * jj + jnow : 3 * jj + 3 + jnow] for jj in range(num_unitaries)
            ]
            jnow += 3 * num_unitaries

            # build unitary corresponding to layer + gradients
            matL, uLlist = get_u4_layer(layer_parameters, do_first, num_qubits)

        else:  # u2 layer
            if u2skip and u2_idx % 2:  # odd layer
                num_unitaries = num_qubits - 2
                do_first = False
                # hack to ensure last layer is full (for 4qubit - 57 angles model)
                if u2_idx == u2layers - 1:
                    num_unitaries += 2
                    do_first = True
            else:  # even layer
                num_unitaries = num_qubits
                do_first = True

            u2_idx += 1

            # get parameters
            layer_parameters = [
                [
                    vec_sin[3 * jj + jnow : 3 * jj + 3 + jnow],
                    vec_cos[3 * jj + jnow : 3 * jj + 3 + jnow],
                ]
                for jj in range(num_unitaries)
            ]
            jnow += 3 * num_unitaries

            # build unitary corresponding to layer + gradients
            matL, uLlist = get_u2_layer(layer_parameters, do_first, num_qubits)

        vlist_new = []
        for kk in range(len(vlist_now)):
            fvec = matL.dot(vlist_now[kk])
            vlist_new.append(fvec)

        # get gradients of A matrix
        for kk in range(len(uLlist)):
            fvec = uLlist[kk].dot(vlist_now[0])
            vlist_new.append(fvec)

        vlist_now = vlist_new

    vlist = vlist_now

    vec_out = np.real(vlist[0] * vlist[0].conjugate())
    if do_grad:  # compute gradient
        grad = np.zeros((rsize, len(angles)))
        for aa in range(len(angles)):
            grad[:, aa] = 2 * np.real(vlist[0].conjugate() * vlist[aa + 1])

        return vec_out, grad

    return vec_out


def get_u2_layer(layer_parameters, do_first, nqubits):
    # skip the first and last qubit while dropping 2 unitaries for odd layers
    if do_first:  # even layer
        num_unitaries = nqubits
    else:  # odd layer
        num_unitaries = nqubits - 2

    # build first unitary
    pnow = layer_parameters[0]
    matF, uFlist = get_u2_mat_and_grad(pnow)

    if do_first:
        matL = matF
        uLlist = uFlist
        size_now = 2
    else:
        # start layer with an identity on first qubit
        matL = p0
        uLlist = []
        size_now = 2
        # then apply first non trivial unitary
        size_now = size_now << 1
        matL, uLlist = append_unitary(matL, uLlist, matF, uFlist, size_now)

    # now do the rest
    for qq in range(num_unitaries - 1):
        # compute next unitary
        pnow = layer_parameters[qq + 1]
        mat_now, ulist_now = get_u2_mat_and_grad(pnow)
        # increase size
        size_now = size_now << 1
        # append to those before
        matL, uLlist = append_unitary(matL, uLlist, mat_now, ulist_now, size_now)

    # add last identity
    # TODO: add fix to deal with odd number of qubits
    if not do_first:
        size_now = size_now << 1
        matL, uLlist = append_unitary(matL, uLlist, p0, [], size_now)

    return matL, uLlist


def get_u4_layer(layer_parameters, do_first, nqubits):

    # skip the first and last qubit while dropping a unitary for odd layers
    if do_first:  # even layer
        num_unitaries = nqubits // 2
    else:  # odd layer
        num_unitaries = nqubits // 2 - 1
    # build first unitary
    pnow = layer_parameters[0]
    matF = get_basic_u4(pnow)

    # -- perform gradients by hand
    uFlist = []
    pnow[0] += 0.5 * np.pi
    dmatC = get_basic_u4(pnow)
    pnow[0] -= 0.5 * np.pi
    uFlist.append(dmatC)
    pnow[1] += 0.5 * np.pi
    dmatC = get_basic_u4(pnow)
    pnow[1] -= 0.5 * np.pi
    uFlist.append(dmatC)
    pnow[2] += 0.5 * np.pi
    dmatC = get_basic_u4(pnow)
    pnow[2] -= 0.5 * np.pi
    uFlist.append(dmatC)

    if do_first:
        matL = matF
        uLlist = uFlist
        size_now = 4
    else:
        # start layer with an identity on first qubit
        matL = p0
        uLlist = []
        size_now = 2
        # then apply first non trivial unitary
        size_now = size_now << 2
        matL, uLlist = append_unitary(matL, uLlist, matF, uFlist, size_now)

    # now do the rest
    for qq in range(num_unitaries - 1):
        # compute next unitary
        params_C = layer_parameters[qq + 1]
        matC = get_basic_u4(params_C)
        # -- perform gradients by hand
        uClist = []
        params_C[0] += 0.5 * np.pi
        dmatC = get_basic_u4(params_C)
        params_C[0] -= 0.5 * np.pi
        uClist.append(dmatC)
        params_C[1] += 0.5 * np.pi
        dmatC = get_basic_u4(params_C)
        params_C[1] -= 0.5 * np.pi
        uClist.append(dmatC)
        params_C[2] += 0.5 * np.pi
        dmatC = get_basic_u4(params_C)
        params_C[2] -= 0.5 * np.pi
        uClist.append(dmatC)
        # increase size
        size_now = size_now << 2
        # append to those before
        matL, uLlist = append_unitary(matL, uLlist, matC, uClist, size_now)

    # TODO: add fix to deal with odd number of number of qubits
    if not do_first:
        size_now = size_now << 1
        matL, uLlist = append_unitary(matL, uLlist, p0, [], size_now)
    return matL, uLlist


def append_unitary(matA, listA, matB, listB, size_now):
    matC = (matA[:, None, :, None] * matB[None, :, None, :]).reshape(size_now, size_now)
    listC = []
    for mm in listA:
        tmat = (mm[:, None, :, None] * matB[None, :, None, :]).reshape(
            size_now, size_now
        )
        listC.append(tmat)
    for mm in listB:
        tmat = (matA[:, None, :, None] * mm[None, :, None, :]).reshape(
            size_now, size_now
        )
        listC.append(tmat)
    return matC, listC
