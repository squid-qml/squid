SQUID package
=============

SQUID package currently consists only of models.
A more detailed discussion is :doc:`squid.models`, however on a high level, :py:class:`MainModel <squid.models.MainModel>` combines three models: Encoder, Middle Model and Decoder.

Middle Model is the most important part, and it can be either quantum or classical (with corresponding abstract classes :py:class:`QuantumModel <squid.models.QuantumModel>` and :py:class:`ClassicalModel <squid.models.ClassicalModel>`).
It is suppose to perform a differentiable function that can be used for a certain prediction, identification or other tasks.

Encoder's purpose is to scale incoming data so that it can fit into Middle Model.
This is important only in case of Middle Model being quantum, as quantum computers are not large (at least as of April 2021).

Decoder's has the exact opposite task of taking outputs of Middle Model and transforming them into shape that is expected.
Again, it is more useful in quantum case, as number of qubits in quantum circuits does not change.
Hence an appropriate scaling up and down is necessary.

.. note:: Typically in QML literature only a few qubits of the output are used for given task. This might lead to significant loss of information, which squid is trying to avoid through use of Decoder. However, taking result of first qubit is still possible as indexing of an array (or matrix) is differentiable.

.. toctree::
   :maxdepth: 4

   squid.models
