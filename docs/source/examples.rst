Examples
==============

All of the examples are available in `SQUID repo examples folder <https://bitbucket.org/squid-qml/squid/src/master/examples/>`_.

Classical
---------

Fully Connected
^^^^^^^^^^^^^^^
.. literalinclude:: ../../examples/classical_1_3_connected.py
   :language: python


Convolutional
^^^^^^^^^^^^^
.. literalinclude:: ../../examples/classical_1_conv_3_connected.py
   :language: python


Quantum
-------

Fully Connected + VQC
^^^^^^^^^^^^^^^^^^^^^
.. literalinclude:: ../../examples/classical_1_conv_3_connected.py
   :language: python

Convolutional + VQC
^^^^^^^^^^^^^^^^^^^
.. literalinclude:: ../../examples/classical_1_conv_3_connected.py
   :language: python
