Quantum Utilities
=================

This package contains quantum utilities that are used primarily by quantum models.

VQC Mod
-------------------------------------------

.. automodule:: squid.models.quantum_utils.vqc_mod
   :members:
   :undoc-members:
   :show-inheritance:

VQC Unitaries module
-------------------------------------------------

.. automodule:: squid.models.quantum_utils.vqc_unitaries
   :members:
   :undoc-members:
   :show-inheritance:
