Classical Utilities
===================

This package contains classical utilities that are used by both classical and quantum models.

str\_to\_activation
------------------------------

.. automethod:: squid.models.classical_utils.str_to_activation

ToRange
-----------------

.. autoclass:: squid.models.classical_utils.ToRange
   :members:
   :undoc-members:
   :show-inheritance:
