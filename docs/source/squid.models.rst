Models
====================
.. currentmodule:: squid.models


Models package is central to SQUID.
It contains :py:class:`MainModel` around which whole package is built, as well as defines classes that can be it's building blocks.

As mentioned in :doc:`squid` MainModel consists of three parts: Encoder, Middle Model and Decoder.
Encoder and Decoder are both :ref:`Classical Models`, or some custom subclass of :py:class:`ClassicalModel`.
Middle Model on the other hand has to be differentiable. This means that any function, quantum or classical that has a defined derivative can be used here.
Currently two types of such functions: classical, mentioned earlier, and :py:class:`QuantumModel`, with corresponding implementations in :ref:`Quantum Models`.

All of the models are shown on this page, and in summary below. Additionally, :ref:`Abstract Models` are provided in bottom, for further development. Lastly, utilities (both quantum and classical) are documented at the end of this page.

.. autosummary::
    MainModel
    ConvFeedForward
    FeedForward
    SelectSingleOutput
    VQC

Main Model
----------

.. autoclass:: MainModel
    :members:

Available Models
----------------

Classical Models
^^^^^^^^^^^^^^^^

.. autoclass:: ConvFeedForward

    .. automethod:: _tuple_to_conv


.. autoclass:: FeedForward

.. autoclass:: SelectSingleOutput

Quantum Models
^^^^^^^^^^^^^^

.. autoclass:: VQC

    .. automethod:: clip


Abstract Models
---------------

.. autoclass:: ClassicalModel

.. autoclass:: QuantumModel

    .. automethod:: clip


Utilities
---------

.. toctree::
    :maxdepth: 2

    squid.models.classical_utils
    squid.models.quantum_utils
