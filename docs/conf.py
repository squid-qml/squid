# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import shutil
dir_path = os.path.abspath('..')
sys.path.insert(0, os.path.abspath('..'))
sys.path.insert(0, os.path.abspath('.'))

# shutil.copy(os.path.join(here_path, "../README.md"), os.path.join(here_path, "source/README.md"))

# -- Project information -----------------------------------------------------

project = 'SQUID'
copyright = '2021, Jakub Filipek, Alessandro Roggero'
author = 'Jakub Filipek, Alessandro Roggero'


# -- General configuration ---------------------------------------------------

# Read the Docs defaults to contents
master_doc = 'index'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinx_rtd_theme",
    "m2r2",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

source_suffix = [".rst", ".md"]


def maybe_skip_member(app, what, name, obj, skip, options):
    if name in ("main_model", "MainModel"):
        print("--------------------")
        print("SEARCHME MEMBER SKIP")
        print(f"App: {app}")
        print(f"What: {what}")
        print(f"Name: {name}")
        print(f"Obj: {obj}")
        print(f"Skip: {skip}")
        print(f"Options: {options}")
    return True


# Modify Read The Docs default a lil
def setup(app):
    app.add_css_file('my_theme.css')
    app.connect('autodoc-skip-member', maybe_skip_member)


# def remove_copied_files(app, exception):
#     try:  # ReadTheDocs fails with this command, but it works when used locally
#         os.remove('source/README.md')
#     except:  # noqa: E722
#         pass
