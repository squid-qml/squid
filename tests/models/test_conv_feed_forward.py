from unittest.mock import Mock

import pytest
import torch
from torch import nn

from squid import models


@pytest.fixture
def patch_path():
    return "squid.models.conv_feed_forward"


@pytest.fixture
def patch_nn(mocker, patch_path):

    return mocker.patch(
        f"{patch_path}.nn",
        Mock(
            Linear=Mock(side_effect=lambda x, y: (x, y)),
            Sequential=Mock(side_effect=lambda *args: sequential_mock(*args)),
            Module=Mock(),
        ),
    )


@pytest.fixture
def patch_str_to_activation(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.str_to_activation", Mock(side_effect=lambda l: l)
    )


@pytest.fixture
def patch_tuple_to_conv(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.ConvFeedForward._tuple_to_conv",
        Mock(side_effect=lambda prev_channels, tup: (prev_channels, tup)),
    )


def sequential_mock(*args, **kwargs):
    class SM:
        def __init__(self, *args, **kwargs):
            self.args = list(args)

        def __call__(self, *args, **kwargs):
            return self

        @property
        def shape(self):
            return [1, 32, 32]

        @property
        def as_expected(self):
            return self.args

    return SM(*args, **kwargs)


def get_mocked_model(layers):
    x = Mock(layers=layers)
    result = Mock(
        forward=Mock(side_effect=lambda l: models.ConvFeedForward.forward(x, l))
    )
    return result


def get_linear_model_ones(in_features, out_features):
    layer = nn.Linear(in_features, out_features, False)
    layer.weight = nn.Parameter(torch.ones(out_features, in_features))
    return layer


@pytest.mark.parametrize(
    "in_shape, conv_layers, layers, kwargs, expected_conv_layers, expected_layers",
    [
        ([32, 32], [], [], {}, nn.Sequential(), nn.Sequential()),
        (
            [1, 32, 32],
            [("C2", 1, 1, 3), ("M2", 2), "sigmoid"],
            [],
            {},
            [
                nn.Conv2d(1, 1, kernel_size=(1, 1), stride=(3, 3)),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1),
                nn.Sigmoid(),
            ],
            nn.Sequential(),
        ),
        (
            [1, 32, 32],
            [("C2", 1, 3), ("M2", 2), "sigmoid"],
            [],
            {"num_classes": 3},
            [
                nn.Conv2d(1, 1, kernel_size=(3, 3), stride=(1, 1)),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1),
                nn.Sigmoid(),
            ],
            nn.Sequential(nn.Linear(15 * 15, 3)),
        ),
        (
            [1, 32, 32],
            [],
            [5, "tanh", 4],
            {"num_classes": 2},
            nn.Sequential(),
            nn.Sequential(
                nn.Linear(32 * 32, 5), nn.Tanh(), nn.Linear(5, 4), nn.Linear(4, 2),
            ),
        ),
        (
            [1, 32, 32],
            [("C2", 1, 3), ("M2", 2), "sigmoid"],
            [5, "tanh", 4],
            {},
            [
                nn.Conv2d(1, 1, kernel_size=(3, 3), stride=(1, 1)),
                nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1),
                nn.Sigmoid(),
            ],
            nn.Sequential(nn.Linear(15 * 15, 5), nn.Tanh(), nn.Linear(5, 4)),
        ),
    ],
)
def test_feed_forward_fields_are_set(
    in_shape, conv_layers, layers, kwargs, expected_conv_layers, expected_layers,
):
    model = models.ConvFeedForward(in_shape, conv_layers, layers, **kwargs)

    if expected_layers is None:
        assert model.layers is None
    else:
        assert model.layers is not None
        if hasattr(expected_layers, "__iter__"):
            assert len(expected_layers) == len(model.layers)
            for result_layer, expected_layer in zip(model.layers, expected_layers):
                assert isinstance(expected_layer, type(result_layer))
                if isinstance(result_layer, nn.Linear):
                    assert result_layer.in_features == expected_layer.in_features
                    assert result_layer.out_features == expected_layer.out_features
                    assert (result_layer.bias is None) == (expected_layer is None)
        else:
            assert isinstance(expected_layers, type(model.layers))
            if isinstance(model.layers, nn.Linear):
                assert model.layers.in_features == expected_layers.in_features
                assert model.layers.out_features == expected_layers.out_features
                assert (model.layers.bias is None) == (expected_layers is None)

    if expected_conv_layers is None:
        assert model.conv_layers is None
    else:
        assert model.conv_layers is not None
        if hasattr(expected_conv_layers, "__iter__"):
            assert len(expected_conv_layers) == len(model.conv_layers)
            for result_layer, expected_layer in zip(
                model.conv_layers, expected_conv_layers
            ):
                assert isinstance(expected_layer, type(result_layer))
                if isinstance(result_layer, (nn.Conv2d, nn.MaxPool2d)):
                    assert result_layer.kernel_size == expected_layer.kernel_size
                    assert result_layer.stride == expected_layer.stride
                    assert result_layer.padding == expected_layer.padding
                    assert result_layer.dilation == expected_layer.dilation
                    assert result_layer.training == expected_layer.training
                    if isinstance(result_layer, nn.Conv2d):
                        assert result_layer.padding_mode == expected_layer.padding_mode
                        assert (
                            result_layer.output_padding == expected_layer.output_padding
                        )
                        assert result_layer.transposed == expected_layer.transposed
                        assert (result_layer.bias is None) == (expected_layer is None)
        else:
            assert isinstance(expected_conv_layers, type(model.conv_layers))
            if isinstance(model.conv_layers, (nn.Conv2d, nn.MaxPool2d)):
                assert model.conv_layers.kernel_size == expected_conv_layers.kernel_size
                assert model.conv_layers.stride == expected_conv_layers.stride
                assert model.conv_layers.padding == expected_conv_layers.padding
                assert model.conv_layers.dilation == expected_conv_layers.dilation
                assert model.conv_layers.training == expected_conv_layers.training
                if isinstance(model.conv_layers, nn.Conv2d):
                    assert (
                        model.conv_layers.padding_mode
                        == expected_conv_layers.padding_mode
                    )
                    assert (
                        model.conv_layers.output_padding
                        == expected_conv_layers.output_padding
                    )
                    assert (
                        model.conv_layers.transposed == expected_conv_layers.transposed
                    )
                    assert (model.conv_layers.bias is None) == (
                        expected_conv_layers is None
                    )


@pytest.mark.parametrize(
    "in_shape, conv_layers, layers, kwargs, expected_shape",
    [
        ([32, 32], [], [], {}, [32 * 32]),
        ([1, 32, 32], [("C2", 1, 1, 3), ("M2", 2), "sigmoid"], [], {}, [25]),
        (
            [1, 32, 32],
            [("C2", 1, 3), ("M2", 2), "sigmoid"],
            [],
            {"num_classes": 3},
            [3],
        ),
        ([1, 32, 32], [], [5, "tanh", 4], {"num_classes": 2}, [2]),
        ([1, 32, 32], [("C2", 1, 3), ("M2", 2), "sigmoid"], [5, "tanh", 4], {}, [4]),
    ],
)
def test_feed_forward_members_are_set(
    in_shape, conv_layers, layers, kwargs, expected_shape,
):
    model = models.ConvFeedForward(in_shape, conv_layers, layers, **kwargs)

    result = model(torch.randn([2] + list(in_shape), requires_grad=True))

    assert list(result.shape[1:]) == expected_shape


@pytest.mark.parametrize(
    "prev_channels, tup, expected, expected_new_channels,",
    [
        (3, ("C1", 1, 3), nn.Conv1d(3, 1, 3), 1),
        (3, ("C2", 1, 3), nn.Conv2d(3, 1, 3), 1),
        (3, ("C3", 1, 3), nn.Conv3d(3, 1, 3), 1),
        (3, ("CT1", 1, 3), nn.ConvTranspose1d(3, 1, 3), 1),
        (3, ("CT2", 1, 3), nn.ConvTranspose2d(3, 1, 3), 1),
        (3, ("CT3", 1, 3), nn.ConvTranspose3d(3, 1, 3), 1),
        (3, ("M1", 2), nn.MaxPool1d(2), 3),
        (3, ("M2", 2), nn.MaxPool2d(2), 3),
        (3, ("M3", 2), nn.MaxPool3d(2), 3),
    ],
)
def test_tuple_to_conv(prev_channels, tup, expected, expected_new_channels):
    result, new_channels = models.ConvFeedForward._tuple_to_conv(prev_channels, tup)

    assert expected_new_channels == new_channels
    assert isinstance(expected, type(result))

    assert result.kernel_size == expected.kernel_size
    assert result.stride == expected.stride
    assert result.padding == expected.padding
    assert result.dilation == expected.dilation
    assert result.training == expected.training
    if isinstance(result, (nn.Conv1d, nn.Conv2d, nn.Conv3d)):
        assert result.padding_mode == expected.padding_mode
        assert result.output_padding == expected.output_padding
        assert result.transposed == expected.transposed
        assert (result.bias is None) == (expected is None)
