from unittest.mock import Mock, sentinel

import pytest
import torch

from squid import models


@pytest.fixture
def patch_path():
    return "squid.models.select_single_output"


@pytest.fixture
def patch_nn(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.nn",
        Mock(
            Linear=Mock(side_effect=lambda x, y: (x, y)),
            Sequential=Mock(side_effect=lambda *args: list(args)),
            Module=Mock(),
        ),
    )


@pytest.mark.parametrize(
    "qubit_idx, use_linear_after, num_classes, expected_qubit_idx, expected_fc,",
    [
        (2, False, 4, tuple([2]), None,),
        ([4, 1, 2], True, 4, (4, 1, 2), (3, 4),),
        ([3, 2, 1, 4, 7], True, None, (3, 2, 1, 4, 7), (5, 5),),
    ],
)
def test_select_single_output_fields_are_set(
    patch_nn, qubit_idx, use_linear_after, num_classes, expected_qubit_idx, expected_fc,
):
    model = models.SelectSingleOutput(qubit_idx, use_linear_after, num_classes)

    assert model.qubit_idx == expected_qubit_idx
    if expected_fc is None:
        assert not hasattr(model, "fc")
    else:
        assert model.fc == expected_fc


@pytest.mark.parametrize(
    "x, qubit_idx, use_linear_after, num_classes, expected_result",
    [
        (
            torch.ones((10, 5)),
            [2, 3],
            False,
            3,
            torch.tensor([[2.0, -1.0] for _ in range(10)]),
        ),
        (torch.ones((10, 5)), [2, 3], True, 3, sentinel.output_3,),
    ],
)
def test_forward(
    patch_nn, x, qubit_idx, use_linear_after, num_classes, expected_result,
):
    model = models.SelectSingleOutput(qubit_idx, use_linear_after, num_classes)

    if use_linear_after:
        model.fc = Mock(return_value=getattr(sentinel, f"output_{num_classes}"))

    result = model(x)

    if isinstance(expected_result, torch.Tensor):
        assert torch.allclose(result, expected_result)
    else:
        assert result == expected_result
