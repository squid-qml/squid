from unittest.mock import Mock

import pytest
import torch

from squid import models


@pytest.fixture
def patch_path():
    return "squid.models.feed_forward"


@pytest.fixture
def patch_nn(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.nn",
        Mock(
            Linear=Mock(side_effect=lambda x, y: (x, y)),
            Sequential=Mock(side_effect=lambda *args: list(args)),
            Module=Mock(),
        ),
    )


@pytest.fixture
def patch_str_to_activation(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.str_to_activation", Mock(side_effect=lambda l: l)
    )


def get_mocked_model(layers):
    x = Mock(layers=layers)
    result = Mock(forward=Mock(side_effect=lambda l: models.FeedForward.forward(x, l)))
    return result


def get_linear_model_ones(in_features, out_features):
    layer = torch.nn.Linear(in_features, out_features, False)
    layer.weight = torch.nn.Parameter(torch.ones(out_features, in_features))
    return layer


@pytest.mark.parametrize(
    "layers, in_features, kwargs, expected",
    [
        ([], 5, {}, []),
        (
            [4, "relu", 3, "sigmoid", 2, "some other activation", 1, "to range"],
            5,
            {"num_classes": 10},
            [
                (5, 4),
                "relu",
                (4, 3),
                "sigmoid",
                (3, 2),
                "some other activation",
                (2, 1),
                "to range",
                (1, 10),
            ],
        ),
        (["relu"], 5, {"num_classes": 10}, ["relu", (5, 10)]),
    ],
)
def test_feed_forward_fields_are_set(
    patch_str_to_activation, patch_nn, layers, in_features, kwargs, expected
):
    model = models.FeedForward(layers, in_features, **kwargs)

    assert model.layers == expected


@pytest.mark.parametrize(
    "forward_input, layers, expected",
    [
        (
            torch.tensor([1.0, 2.0, 3.0]),
            torch.nn.Sequential(),
            torch.tensor([1.0, 2.0, 3.0]),
        ),
        (
            torch.tensor([[1.0, 2.0, 3.0], [2.0, 4.0, 6.0]]),
            torch.nn.Sequential(get_linear_model_ones(3, 2)),
            torch.tensor([[6.0, 6.0], [12.0, 12.0]]),
        ),
    ],
)
def test_forward(forward_input, layers, expected):
    model = get_mocked_model(layers)

    result = model.forward(forward_input)
    assert torch.equal(result, expected)
