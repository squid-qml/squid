import numpy as np
import pytest

from squid.models.quantum_utils import vqc_mod


@pytest.mark.parametrize(
    "num_angles, num_qubits, expected_u2, expected_u4, expected_error",
    [
        (3, 1, 1, 0, False),
        (3, 2, 1, 0, True),
        (6, 2, 1, 0, False),
        (6, 1, 1, 0, True),
        (9, 2, 1, 1, False),
        (9, 1, 1, 1, True),
        (12, 4, 1, 0, False),
        (12, 2, 1, 0, True),
        (15, 2, 2, 1, False),
        (15, 4, 2, 1, True),
        (18, 6, 1, 0, False),
        (18, 4, 1, 0, True),
        (30, 4, 2, 1, False),
        (30, 6, 2, 1, True),
        (39, 4, 3, 2, False),
        (39, 6, 3, 2, True),
        (45, 4, 3, 2, True),
        (45, 2, 3, 2, True),
        (45, 6, 2, 1, False),
        (45, 8, 2, 1, True),
        (57, 4, 4, 3, False),
        (57, 6, 4, 3, True),
        (63, 6, 4, 3, True),
        (49, 6, 4, 3, True),
    ],
)
def test_validate_model(
    num_angles, num_qubits, expected_u2, expected_u4, expected_error,
):
    try:
        result_u2, result_u4 = vqc_mod.validate_model(num_angles, num_qubits)

        assert expected_u2 == result_u2
        assert expected_u4 == result_u4

        assert not expected_error
    except ValueError as e:
        if not expected_error:
            raise e


@pytest.mark.parametrize(
    "angles, vqc_params, do_grad, expected, expected_error",
    [
        (np.random.randn(8), [9, 2, 1, 1], False, None, True),
        (np.random.randn(9), [9, 2, 1, 1], False, [4], False),
        (np.random.randn(9), [9, 2, 1, 1], True, ([4], [4, 9]), False),
        (np.random.randn(39), [39, 4, 3, 2], False, ([16]), False),
        (np.random.randn(45), [45, 6, 2, 1], False, [64], False),
        (np.random.randn(45), [45, 6, 2, 1], True, ([64], [64, 45]), False),
        (np.random.randn(57), [57, 4, 4, 3], True, ([16], [16, 57]), False),
    ],
)
def test_measure_vqc_output(angles, vqc_params, do_grad, expected, expected_error):
    try:
        result = vqc_mod.measure_vqc_output(angles, vqc_params, do_grad)

        assert not expected_error
    except ValueError as e:
        if not expected_error:
            raise e
        else:
            return

    if do_grad:
        assert list(result[0].shape) == expected[0]
        assert list(result[1].shape) == expected[1]
    else:
        assert list(result.shape) == expected
