import numpy as np
import pytest

from squid.models.quantum_utils import vqc_mod


@pytest.mark.parametrize(
    "params, expected",
    [
        (
            [1.0, 1.0, 1.0],
            np.array(
                [
                    [0.54030231 + 0.84147098j, 0.0 + 0.0j, 0.0 + 0.0j, 0.0 + 0.0j],
                    [
                        0.0 + 0.0j,
                        -0.2248451 + 0.35017549j,
                        0.7651474 + 0.4912955j,
                        0.0 + 0.0j,
                    ],
                    [
                        0.0 + 0.0j,
                        0.7651474 + 0.4912955j,
                        -0.2248451 + 0.35017549j,
                        0.0 + 0.0j,
                    ],
                    [0.0 + 0.0j, 0.0 + 0.0j, 0.0 + 0.0j, 0.54030231 + 0.84147098j],
                ],
            ),
        ),
    ],
)
def test_get_basic_u4(params, expected):
    result = vqc_mod.get_basic_u4(params)

    expected = np.array(expected)
    assert result.shape == expected.shape
    assert np.allclose(result, expected)


@pytest.mark.parametrize(
    "params, expected",
    [([[1.0, 0.5, 0.25], [-1.0, -0.5, -0.25]], ([2, 2], [[2, 2], [2, 2], [2, 2]]))],
)
def test_u2_mat_and_grad(params, expected):
    result = vqc_mod.get_u2_mat_and_grad(params)

    assert list(result[0].shape) == expected[0]
    assert [list(x.shape) for x in result[1]] == expected[1]
