from unittest.mock import sentinel

import pytest
import torch

from squid.models import QuantumModel


@pytest.fixture
def mock_quantum_model_subclass():
    class MockQuantumModel(QuantumModel):
        def forward(self, x):
            return sentinel.forward

        def backward(self, x):
            return sentinel.backward

    return MockQuantumModel()


def test_quantum_model_call(mock_quantum_model_subclass):
    assert mock_quantum_model_subclass(None) == sentinel.forward


def test_quantum_model_clip(mock_quantum_model_subclass):
    r = torch.randn((10, 5))
    assert torch.allclose(mock_quantum_model_subclass.clip(r), r)
