from typing import Any, List
from unittest.mock import Mock, call, sentinel

import pytest
import torch

from squid.models import ClassicalModel, MainModel, QuantumModel


@pytest.fixture
def patch_path():
    return "squid.models.main_model"


@pytest.fixture
def patch_forward(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.MainModel.forward", Mock(return_value=sentinel.forward),
    )


@pytest.fixture
def patch_model_vqc(mocker, patch_path):
    return mocker.patch(f"{patch_path}.VQC", Mock(return_value=sentinel.VQC))


@pytest.fixture
def patch_model_conv_feed_forward(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.ConvFeedForward", Mock(return_value=sentinel.ConvFeedForward),
    )


@pytest.fixture
def patch_model_feed_forward(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.FeedForward", Mock(return_value=sentinel.FeedForward),
    )


@pytest.fixture
def patch_model_select_single_output(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.SelectSingleOutput",
        Mock(return_value=sentinel.SelectSingleOutput),
    )


@pytest.fixture
def patch_warnings_warn(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.warnings.warn", Mock(return_value=sentinel.warning),
    )


@pytest.fixture
def patch_torch_load(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.torch.load",
        Mock(
            return_value={
                "model1.layer1": sentinel.layer11,
                "model1.layer2": sentinel.layer12,
                "model1.layer3": sentinel.layer13,
                "model2.layer1": sentinel.layer21,
                "model2.layer2": sentinel.layer22,
                "model2.layer3": sentinel.layer23,
                "model3.layer1": sentinel.layer31,
                "model3.layer2": sentinel.layer32,
                "model3.layer3": sentinel.layer33,
            },
        ),
    )


@pytest.fixture
def patch_np_random(mocker, patch_path):
    return (
        mocker.patch(
            f"{patch_path}.np.random.get_state",
            Mock(return_value=sentinel.np_random_state),
        ),
        mocker.patch(
            f"{patch_path}.np.random.seed", Mock(return_value=sentinel.np_random_state),
        ),
        mocker.patch(
            f"{patch_path}.np.random.set_state",
            Mock(return_value=sentinel.np_random_state),
        ),
    )


@pytest.fixture
def patch_torch_random(mocker, patch_path):
    return (
        mocker.patch(
            f"{patch_path}.torch.random.get_rng_state",
            Mock(return_value=sentinel.torch_random_state),
        ),
        mocker.patch(
            f"{patch_path}.torch.random.manual_seed",
            Mock(return_value=sentinel.torch_random_state),
        ),
        mocker.patch(
            f"{patch_path}.torch.random.set_rng_state",
            Mock(return_value=sentinel.torch_random_state),
        ),
    )


@pytest.fixture
def patch_random(mocker, patch_path):
    return (
        mocker.patch(
            f"{patch_path}.random.getstate", Mock(return_value=sentinel.random_state),
        ),
        mocker.patch(
            f"{patch_path}.random.seed", Mock(return_value=sentinel.random_state),
        ),
        mocker.patch(
            f"{patch_path}.random.setstate", Mock(return_value=sentinel.random_state),
        ),
    )


@pytest.fixture
def sentinel_mock_main_model():
    return MainModel(sentinel.model1, sentinel.model2, sentinel.model3)


@pytest.fixture
def sentinel_mock_quantum_model_subclass():
    return SentinelMockQuantumModel()


@pytest.fixture
def sentinel_mock_classical_model_subclass():
    return SentinelMockClassicalModel()


@pytest.fixture
def simple_mock_quantum_model_subclass():
    return SimpleMockQuantumModel()


@pytest.fixture
def simple_mock_classical_model_subclass():
    return SimpleMockClassicalModel()


# region: helpers


class SentinelMockQuantumModel(QuantumModel):
    def forward(self, x):
        return sentinel.quantum_forward

    def backward(self, x):
        return sentinel.quantum_backward


class SentinelMockClassicalModel(ClassicalModel):
    devices: List[Any] = []

    def forward(self, x):
        return sentinel.classical_forward

    def to(self, device):
        self.devices.append(device)
        return self


class SimpleMockQuantumModel(QuantumModel):
    def clip(self, x):
        return x / 2

    def forward(self, x):
        return x

    def backward(self, x):
        return x

    load_state_dict = Mock(side_effect=lambda x: x)


class SimpleMockClassicalModel(ClassicalModel):
    def forward(self, x):
        return x

    load_state_dict = Mock(side_effect=lambda x: x)


def list_to_parameters(param_list):
    return Mock(return_value=[Mock(value=x, requires_grad=y) for x, y in param_list])


def get_model(model_type: str):
    if model_type.lower() in [
        "simplemockquantummodel",
        "simple_mock_quantum_model",
        "quantum",
    ]:
        return SimpleMockQuantumModel()
    else:
        return SimpleMockClassicalModel()


def raise_value_error():
    raise ValueError()


# end region: helpers


@pytest.mark.parametrize(
    "model_1, model_2, model_3, config, expected_models, expected_trains",
    [
        (
            sentinel.model_1,
            sentinel.model_2,
            sentinel.model_3,
            None,
            (sentinel.model_1, sentinel.model_2, sentinel.model_3),
            (True, True, True),
        ),
        (
            sentinel.model_1,
            sentinel.model_2,
            sentinel.model_3,
            {
                "model 1": {"fixed": True},
                "model 2": {"fixed": True},
                "model 3": {"fixed": True},
            },
            (sentinel.model_1, sentinel.model_2, sentinel.model_3),
            (False, True, False),
        ),
        (
            sentinel.model_1,
            "sentinel_mock_classical_model_subclass",
            sentinel.model_3,
            {
                "model 1": {"fixed": False},
                "model 2": {"fixed": True},
                "model 3": {"fixed": True},
            },
            (
                sentinel.model_1,
                "sentinel_mock_classical_model_subclass",
                sentinel.model_3,
            ),
            (True, False, False),
        ),
    ],
)
def test_main_model_fields_are_set(
    sentinel_mock_classical_model_subclass,
    model_1,
    model_2,
    model_3,
    config,
    expected_models,
    expected_trains,
):
    if isinstance(model_2, str):
        model_2 = vars()[model_2]
    if isinstance(expected_models[1], str):
        expected_models = (
            expected_models[0],
            vars()[expected_models[1]],
            expected_models[2],
        )

    model = MainModel(model_1, model_2, model_3, config=config,)
    assert expected_models == (model.model1, model.model2, model.model3)
    assert expected_trains == (
        model.model1_train,
        model.model2_train,
        model.model3_train,
    )


@pytest.mark.parametrize(
    "model_1, model_2, model_3, config, trainable_only, expected",
    [
        (
            [(sentinel.param_11, False), (sentinel.param_12, True)],
            (None, [(sentinel.param_21, True), (sentinel.param_22, False)]),
            [(sentinel.param_31, True), (sentinel.param_32, False)],
            None,
            False,
            [
                (sentinel.param_11, False),
                (sentinel.param_12, True),
                (sentinel.param_31, True),
                (sentinel.param_32, False),
            ],
        ),
        (
            [(sentinel.param_11, False), (sentinel.param_12, True)],
            (
                "sentinel_mock_classical_model_subclass",
                [(sentinel.param_21, True), (sentinel.param_22, False)],
            ),
            [(sentinel.param_31, True), (sentinel.param_32, False)],
            None,
            False,
            [
                (sentinel.param_11, False),
                (sentinel.param_12, True),
                (sentinel.param_21, True),
                (sentinel.param_22, False),
                (sentinel.param_31, True),
                (sentinel.param_32, False),
            ],
        ),
        (
            [(sentinel.param_11, False), (sentinel.param_12, True)],
            (
                "sentinel_mock_classical_model_subclass",
                [(sentinel.param_21, True), (sentinel.param_22, False)],
            ),
            [(sentinel.param_31, True), (sentinel.param_32, False)],
            {
                "model 1": {"fixed": True},
                "model 2": {"fixed": False},
                "model 3": {"fixed": False},
            },
            True,
            [(sentinel.param_21, True), (sentinel.param_31, True)],
        ),
        (
            [(sentinel.param_11, False), (sentinel.param_12, True)],
            (
                "sentinel_mock_classical_model_subclass",
                [(sentinel.param_21, True), (sentinel.param_22, False)],
            ),
            [(sentinel.param_31, True), (sentinel.param_32, False)],
            {
                "model 1": {"fixed": False},
                "model 2": {"fixed": True},
                "model 3": {"fixed": True},
            },
            True,
            [(sentinel.param_12, True)],
        ),
    ],
)
def test_parameters(
    sentinel_mock_classical_model_subclass,
    model_1,
    model_2,
    model_3,
    config,
    trainable_only,
    expected,
):
    model_1 = Mock(parameters=list_to_parameters(model_1))
    model_2_type, model_2_args = model_2
    if isinstance(model_2_type, str):
        model_2 = vars()[model_2_type]
        model_2.parameters = list_to_parameters(model_2_args)
    else:
        model_2 = Mock(parameters=list_to_parameters(model_2_args))
    model_3 = Mock(parameters=list_to_parameters(model_3))
    expected = list_to_parameters(expected)()

    model = MainModel(model_1, model_2, model_3, config=config)

    result = model.parameters(trainable_only=trainable_only)

    assert len(result) == len(expected)
    for r, e in zip(result, expected):
        assert r.value == e.value
        assert r.requires_grad == e.requires_grad


@pytest.mark.parametrize(
    "model_2_type, forward_input, expected",
    [
        (
            "simple_mock_classical_model_subclass",
            torch.ones((10, 5)),
            torch.ones((10, 5)),
        ),
        (
            "simple_mock_quantum_model_subclass",
            torch.ones((10, 5)),
            torch.ones((10, 5)) / 2,
        ),
    ],
)
def test_forward(
    simple_mock_quantum_model_subclass,
    simple_mock_classical_model_subclass,
    model_2_type,
    forward_input,
    expected,
):
    model_2 = vars()[model_2_type]
    model = MainModel(
        simple_mock_classical_model_subclass,
        model_2,
        simple_mock_classical_model_subclass,
    )

    result = model.forward(forward_input)
    assert torch.allclose(result, expected)


@pytest.mark.parametrize(
    "model_2_type, forward_input, expected",
    [
        (
            "simple_mock_classical_model_subclass",
            torch.ones((10, 5), requires_grad=True),
            torch.zeros((10, 5)),
        ),
        (
            "simple_mock_quantum_model_subclass",
            torch.ones((20, 5), requires_grad=True),
            -0.005 * torch.ones((20, 5)),
        ),
    ],
)
def test_backward(
    simple_mock_quantum_model_subclass,
    simple_mock_classical_model_subclass,
    model_2_type,
    forward_input,
    expected,
):
    model_2 = vars()[model_2_type]
    model = MainModel(
        simple_mock_classical_model_subclass,
        model_2,
        simple_mock_classical_model_subclass,
    )

    criterion = torch.nn.MSELoss()

    loss = criterion(model.forward(forward_input), torch.ones_like(forward_input))
    loss.backward()

    result = model.backward()

    assert result.shape == expected.shape
    assert torch.allclose(result.data, expected.data)


@pytest.mark.parametrize(
    "args", [(sentinel.args), ([sentinel.args1, sentinel.args2])],
)
def test_call(sentinel_mock_main_model, patch_forward, args):
    result = sentinel_mock_main_model(args)
    assert result == sentinel.forward
    assert patch_forward.mock_calls == [call(args)]


@pytest.mark.parametrize(
    "model_2_type, device, expected_devices",
    [
        (
            "sentinel_mock_quantum_model_subclass",
            sentinel.device,
            [sentinel.device, sentinel.device],
        ),
        (
            "sentinel_mock_classical_model_subclass",
            sentinel.device,
            [sentinel.device, sentinel.device, sentinel.device],
        ),
    ],
)
def test_to(
    sentinel_mock_quantum_model_subclass,
    sentinel_mock_classical_model_subclass,
    model_2_type,
    device,
    expected_devices,
):
    sentinel_mock_classical_model_subclass.devices = []
    model_2 = vars()[model_2_type]
    model = MainModel(
        sentinel_mock_classical_model_subclass,
        model_2,
        sentinel_mock_classical_model_subclass,
    )
    model = model.to(device)

    assert model.model1.devices == expected_devices


@pytest.mark.parametrize(
    "parameters, trainable_only, expected",
    [
        ([torch.ones((10, 10)), torch.randn((10, 5))], False, 150,),
        ([torch.zeros((16, 10)), torch.randn((3, 5))], True, 175,),
    ],
)
def test_num_parameters(
    sentinel_mock_main_model, parameters, trainable_only, expected,
):
    patch_parameters = sentinel_mock_main_model.parameters = Mock(
        return_value=parameters
    )

    result = sentinel_mock_main_model.num_parameters(trainable_only=trainable_only)

    assert result == expected
    assert patch_parameters.mock_calls == [call(trainable_only)]
