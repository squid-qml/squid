from math import pi

import pytest
import torch
from torch import nn

from squid.models.classical_utils import ToRange


@pytest.mark.parametrize(
    "input_range, expected_start, expected_delta",
    [
        (None, 0, pi),
        ((0, 2), 0, 2),
        ((torch.Tensor([4]), 10), 4, 6),
        ((-3, torch.Tensor([3])), -3, 6),
        (torch.Tensor([10, -2]), 10, -12),
    ],
)
def test_to_range_fields_are_set(input_range, expected_start, expected_delta):
    if input_range is None:
        result = ToRange()
    else:
        result = ToRange(input_range)

    assert isinstance(result.sigmoid, nn.Sigmoid)
    assert result.delta == expected_delta
    assert result.start == expected_start


@pytest.mark.parametrize(
    "input_range, expected_min, expected_max",
    [
        (None, 0, pi),
        ((0, 2), 0, 2),
        ((torch.Tensor([4]), 10), 4, 10),
        ((-3, torch.Tensor([3])), -3, 3),
        (torch.Tensor([10, -2]), -2, 10),
    ],
)
def test_to_range_forward(input_range, expected_min, expected_max):
    if input_range is None:
        model = ToRange()
    else:
        model = ToRange(input_range)

    result = model(torch.randn([2, 1000], requires_grad=True))

    assert torch.min(result) >= expected_min
    assert torch.max(result) <= expected_max
