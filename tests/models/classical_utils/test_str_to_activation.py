import pytest
from torch import nn

from squid.models.classical_utils import ToRange, str_to_activation


@pytest.mark.parametrize(
    "activation, expected, expected_error",
    [
        ("relu", nn.ReLU(), False),
        ("ReLU", nn.ReLU(), False),
        ("sigmoid", nn.Sigmoid(), False),
        ("Sigmoid", nn.Sigmoid(), False),
        ("tanh", nn.Tanh(), False),
        ("Tanh", nn.Tanh(), False),
        ("leaky relu", nn.LeakyReLU(), False),
        ("Leaky-ReLU", nn.LeakyReLU(), False),
        ("LEAKY_RELU", nn.LeakyReLU(), False),
        ("angle", ToRange(), False),
        ("To Angle", ToRange(), False),
        ("To_Angle", ToRange(), False),
        ("To-Range", ToRange(), False),
        ("range", ToRange(), False),
        ("invalid", None, True),
        ("reilu", None, True),
        ("Hyperbolic Tangent", None, True),
    ],
)
def test_str_to_activation(activation, expected, expected_error):
    try:
        result = str_to_activation(activation)
        assert isinstance(expected, type(result))
    except ValueError as e:
        if not expected_error:
            raise e
