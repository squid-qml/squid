import math
from unittest.mock import Mock, sentinel

import numpy as np
import pytest
import torch

from squid.models import VQC


@pytest.fixture
def patch_path():
    return "squid.models.vqc_multiq"


@pytest.fixture
def patch_validate_model(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.validate_model", Mock(side_effect=lambda x, y: (x, y)),
    )


@pytest.fixture
def patch_measure_vqc_output(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.measure_vqc_output",
        Mock(side_effect=lambda x, y, do_grad=True: (x, y)),
    )


@pytest.fixture
def patch_np_matmul(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.np.matmul", Mock(side_effect=lambda x, y: (x, y)),
    )


@pytest.mark.parametrize(
    "num_quantum_params, num_qubits, expected_vqc_params",
    [(15, 4, [15, 4, 15, 4]), (9, 1, [9, 1, 9, 1])],
)
def test_vqc_are_set(
    patch_validate_model, num_quantum_params, num_qubits, expected_vqc_params
):
    model = VQC(num_quantum_params, num_qubits)

    assert model.vqc_params == expected_vqc_params
    assert len(model.grad) == 0 and isinstance(model.grad, np.ndarray)


@pytest.mark.parametrize(
    "num_quantum_params, num_qubits, clip_in, expected",
    [
        (15, 4, torch.zeros(40), math.pi * torch.ones(40)),
        (15, 4, torch.zeros((5, 40)), math.pi * torch.ones((5, 40))),
    ],
)
def test_vqc_clip(
    patch_validate_model,
    patch_measure_vqc_output,
    num_quantum_params,
    num_qubits,
    clip_in,
    expected,
):
    model = VQC(num_quantum_params, num_qubits)

    assert torch.allclose(model.clip(clip_in), expected)


@pytest.mark.parametrize(
    "num_quantum_params, num_qubits, forward_in, expected, expected_grad",
    [
        (15, 4, np.ones(40), np.ones(40), [15, 4, 15, 4]),
        (15, 4, np.ones((5, 40)), np.ones((5, 40)), [[15, 4, 15, 4] for _ in range(5)]),
    ],
)
def test_vqc_forward(
    patch_validate_model,
    patch_measure_vqc_output,
    num_quantum_params,
    num_qubits,
    forward_in,
    expected,
    expected_grad,
):
    model = VQC(num_quantum_params, num_qubits)

    result = model.forward(forward_in)

    assert np.allclose(expected, result)
    grad_result = model.grad == expected_grad
    if not isinstance(grad_result, bool):
        grad_result = grad_result.all()
    assert grad_result


@pytest.mark.parametrize(
    "num_quantum_params, num_qubits, prev_grad, grad, expected",
    [
        (15, 4, np.ones(40), None, (np.ones(40), None)),
        (15, 4, np.ones(40), np.zeros(29), (np.ones(40), np.zeros(29))),
    ],
)
def test_vqc_backward(
    patch_validate_model,
    patch_np_matmul,
    num_quantum_params,
    num_qubits,
    prev_grad,
    grad,
    expected,
):
    model = VQC(num_quantum_params, num_qubits)
    model.grad = grad

    result = model.backward(prev_grad)

    assert np.allclose(expected[0], result[0])
    if isinstance(expected[1], np.ndarray):
        assert np.allclose(expected[1], result[1])
    else:
        assert expected[1] == result[1]


def test_vqc_call(patch_validate_model):
    model = VQC(15, 4)
    model.forward = Mock(return_value=sentinel.forward)
    assert model(np.ones((5, 10))) == sentinel.forward
